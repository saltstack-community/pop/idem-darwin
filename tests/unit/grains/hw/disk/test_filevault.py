from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_filevault_enabled(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "FileVault is On."}
    )
    mock_hub.grains.darwin.hw.disk.filevault.load_filevault_enabled = (
        hub.grains.darwin.hw.disk.filevault.load_filevault_enabled
    )
    await mock_hub.grains.darwin.hw.disk.filevault.load_filevault_enabled()
    assert mock_hub.grains.GRAINS.filevault is True


@pytest.mark.asyncio
async def test_load_filevault_disabled(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "FileVault is Off."}
    )
    mock_hub.grains.darwin.hw.disk.filevault.load_filevault_enabled = (
        hub.grains.darwin.hw.disk.filevault.load_filevault_enabled
    )
    await mock_hub.grains.darwin.hw.disk.filevault.load_filevault_enabled()
    assert mock_hub.grains.GRAINS.filevault is False
