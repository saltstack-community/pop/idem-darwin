# import exec.services
import pytest


@pytest.mark.asyncio
async def test_available(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_available_services(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_confirm_updated(
    mock_hub, hub,
):
    pass


@pytest.mark.asyncio
async def test_disable(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_disabled(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_enable(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_enabled(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_get_all(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_get_enabled(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_list_(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_launchctl(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_missing(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_read_plist_file(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_restart(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_show(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_start(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_status(mock_hub, hub):
    pass


@pytest.mark.asyncio
async def test_stop(mock_hub, hub):
    pass


def test_validate_enabled(mock_hub, hub):
    pass
