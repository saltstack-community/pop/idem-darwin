from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_cpu_arch(mock_hub, hub):

    ret = "x86_64"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": ret})
    mock_hub.grains.darwin.hw.cpu.load_cpu_arch = hub.grains.darwin.hw.cpu.load_cpu_arch
    await mock_hub.grains.darwin.hw.cpu.load_cpu_arch()
    assert mock_hub.grains.GRAINS.cpuarch == ret


@pytest.mark.asyncio
async def test_load_cpu_flags(mock_hub, hub):

    ret = (
        "FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE MCA CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE "
        "SSE2 SS HTT TM PBE SSE3 PCLMULQDQ DTES64 MON DSCPL VMX SMX EST TM2 SSSE3 FMA CX16 TPR PDCM SSE4.1 "
        "SSE4.2 x2APIC MOVBE POPCNT AES PCID XSAVE OSXSAVE SEGLIM64 TSCTMR AVX1.0 RDRAND F16C "
    )
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": ret})
    mock_hub.grains.darwin.hw.cpu.load_cpu_flags = (
        hub.grains.darwin.hw.cpu.load_cpu_flags
    )
    await mock_hub.grains.darwin.hw.cpu.load_cpu_flags()
    assert not set(mock_hub.grains.GRAINS.cpu_flags) - set(
        ret.strip().lower().split(" ")
    )


@pytest.mark.asyncio
async def test_load_cpu_model(mock_hub, hub):

    ret = "Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": ret})
    mock_hub.grains.darwin.hw.cpu.load_cpu_model = (
        hub.grains.darwin.hw.cpu.load_cpu_model
    )
    await mock_hub.grains.darwin.hw.cpu.load_cpu_model()
    assert mock_hub.grains.GRAINS.cpu_model == ret


@pytest.mark.asyncio
async def test_load_num_cpus(mock_hub, hub):

    ret = 4
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": str(ret)})
    mock_hub.grains.darwin.hw.cpu.load_num_cpus = hub.grains.darwin.hw.cpu.load_num_cpus
    await mock_hub.grains.darwin.hw.cpu.load_num_cpus()
    assert mock_hub.grains.GRAINS.num_cpus == ret
