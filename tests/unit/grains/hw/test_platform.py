from dict_tools import data
import pytest

PLATFORM_DATA = """
Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro14,1
      Processor Name: Dual-Core Intel Core i5
      Processor Speed: 2.3 GHz
      Number of Processors: 1
      Total Number of Cores: 2
      L2 Cache (per Core): 256 KB
      L3 Cache: 4 MB
      Hyper-Threading Technology: Enabled
      Memory: 8 GB
      Boot ROM Version: 205.0.0.0.0
      SMC Version (system): 2.43f9
      Serial Number (system): ABCD12EFGH34
"""


@pytest.mark.asyncio
async def test_load_profiler(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PLATFORM_DATA})

    mock_hub.grains.darwin.hw.platform.load_profiler = (
        hub.grains.darwin.hw.platform.load_profiler
    )

    await mock_hub.grains.darwin.hw.platform.load_profiler()

    assert mock_hub.grains.GRAINS.model_name == "MacBook Pro"
    assert mock_hub.grains.GRAINS.boot_rom_version == "205.0.0.0.0"
    assert mock_hub.grains.GRAINS.smc_version == "2.43f9"
    assert mock_hub.grains.GRAINS.serialnumber == "ABCD12EFGH34"
