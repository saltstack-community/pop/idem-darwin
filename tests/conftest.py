import os
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    try:
        if os.uname().sysname == "Darwin":
            return hub
    except:
        ...

    pytest.skip("idem-darwin is only intended for MacOS systems")
