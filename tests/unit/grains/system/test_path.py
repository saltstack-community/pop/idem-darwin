import pytest
import sys
import mock


@pytest.mark.asyncio
async def test_load_cwd(mock_hub, hub):

    ret = "test"
    with mock.patch("os.getcwd", return_value=ret):
        mock_hub.grains.darwin.system.path.load_cwd = (
            hub.grains.darwin.system.path.load_cwd
        )
        await mock_hub.grains.darwin.system.path.load_cwd()

    assert mock_hub.grains.GRAINS.cwd == ret


@pytest.mark.asyncio
async def test_load_executable(mock_hub, hub):

    origin = sys.executable
    sys.executable = "test"
    mock_hub.grains.darwin.system.path.load_executable = (
        hub.grains.darwin.system.path.load_executable
    )
    await mock_hub.grains.darwin.system.path.load_executable()
    sys.executable = origin

    assert mock_hub.grains.GRAINS.pythonexecutable == "test"


@pytest.mark.asyncio
async def test_load_path(mock_hub, hub):

    with mock.patch("os.environ.get", return_value="test:test1:test2"):
        mock_hub.grains.darwin.system.path.load_path = (
            hub.grains.darwin.system.path.load_path
        )
        await mock_hub.grains.darwin.system.path.load_path()

    assert mock_hub.grains.GRAINS.path == "test:test1:test2"


@pytest.mark.asyncio
async def test_load_pythonpath(mock_hub, hub):

    with mock.patch("sys.path", ["test", "test1", "test2"]):
        mock_hub.grains.darwin.system.path.load_pythonpath = (
            hub.grains.darwin.system.path.load_pythonpath
        )
        await mock_hub.grains.darwin.system.path.load_pythonpath()

    assert mock_hub.grains.GRAINS.pythonpath == ("test", "test1", "test2")
