import pytest
import mock


@pytest.mark.asyncio
async def test_load_osarch(mock_hub, hub):

    ret = lambda: 0
    ret.machine = "x86_64"
    with mock.patch("os.uname", return_value=ret):
        mock_hub.grains.darwin.os.arch.load_osarch = (
            hub.grains.darwin.os.arch.load_osarch
        )
        await mock_hub.grains.darwin.os.arch.load_osarch()

    assert mock_hub.grains.GRAINS.osarch == ret.machine
