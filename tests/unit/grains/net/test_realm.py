from dict_tools import data
import pytest

DSCONFIGAD_DATA = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Administrative</key>
	<dict>
		<key>Allowed admin groups</key>
		<array>
			<string>domain admins</string>
			<string>enterprise admins</string>
		</array>
		<key>Authentication from any domain</key>
		<true/>
		<key>Namespace mode</key>
		<string>domain</string>
		<key>Packet encryption</key>
		<string>allow</string>
		<key>Packet signing</key>
		<string>allow</string>
		<key>Password change interval</key>
		<integer>14</integer>
	</dict>
	<key>General Info</key>
	<dict>
		<key>Active Directory Domain</key>
		<string>TEST_DOMAIN</string>
		<key>Active Directory Forest</key>
		<string>TEST_FOREST</string>
		<key>Computer Account</key>
		<string>TEST_DEVICE_NAME</string>
	</dict>
	<key>Mappings</key>
	<dict>
		<key>Generate Kerberos authority</key>
		<true/>
	</dict>
	<key>User Experience</key>
	<dict>
		<key>Create mobile account at login</key>
		<true/>
		<key>Force home to startup disk</key>
		<true/>
		<key>Mount home as sharepoint</key>
		<true/>
		<key>Network protocol</key>
		<string>smb</string>
		<key>Require confirmation</key>
		<false/>
		<key>Shell</key>
		<string>/bin/bash</string>
		<key>Use Windows UNC path for home</key>
		<true/>
	</dict>
</dict>
</plist>
"""


@pytest.mark.asyncio
async def test_load_windows_domain(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": DSCONFIGAD_DATA})

    mock_hub.grains.darwin.net.realm.load_windows_domain = (
        hub.grains.darwin.net.realm.load_windows_domain
    )

    await mock_hub.grains.darwin.net.realm.load_windows_domain()

    assert mock_hub.grains.GRAINS.windowsdomain == "TEST_DOMAIN"
    assert mock_hub.grains.GRAINS.windowsdomaintype == "Domain"
