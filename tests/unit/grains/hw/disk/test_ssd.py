from dict_tools import data
import pytest

DISKUTIL_LIST = """
<plist version="1.0">
<dict>
	<key>AllDisks</key>
	<array>
		<string>disk0</string>
		<string>disk0s1</string>
		<string>disk0s2</string>
		<string>disk1</string>
		<string>disk1s1</string>
		<string>disk1s2</string>
		<string>disk1s3</string>
		<string>disk1s4</string>
		<string>disk1s5</string>
	</array>
	<key>WholeDisks</key>
	<array>
		<string>disk0</string>
		<string>disk1</string>
	</array>
</dict>
</plist>
"""

DISKUTIL_HHD = """
<plist version="1.0">
<dict>
	<key>SolidState</key>
	<false/>
</dict>
</plist>
"""

DISKUTIL_SSD = """
<plist version="1.0">
<dict>
	<key>SolidState</key>
	<true/>
</dict>
</plist>
"""


@pytest.mark.asyncio
async def test_load_disks(mock_hub, hub):

    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": DISKUTIL_LIST}),
        data.NamespaceDict({"stdout": DISKUTIL_HHD}),
        data.NamespaceDict({"stdout": DISKUTIL_SSD}),
    ]
    mock_hub.grains.darwin.hw.disk.ssd.load_disks = (
        hub.grains.darwin.hw.disk.ssd.load_disks
    )
    await mock_hub.grains.darwin.hw.disk.ssd.load_disks()
    assert mock_hub.grains.GRAINS.disks == ("disk0", "disk1")
    assert mock_hub.grains.GRAINS.SSDs == ("disk1",)
