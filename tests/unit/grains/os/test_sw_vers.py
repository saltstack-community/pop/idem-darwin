from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_os_build(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "19E266"})

    mock_hub.grains.darwin.os.sw_vers.load_os_build = (
        hub.grains.darwin.os.sw_vers.load_os_build
    )

    await mock_hub.grains.darwin.os.sw_vers.load_os_build()

    assert mock_hub.grains.GRAINS.osbuild == "19E266"


@pytest.mark.asyncio
async def test_load_os_release(mock_hub, hub):

    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": "10.15.4"}),
        data.NamespaceDict({"stdout": "Mac OS X"}),
    ]

    mock_hub.grains.darwin.os.sw_vers.load_os_release = (
        hub.grains.darwin.os.sw_vers.load_os_release
    )

    await mock_hub.grains.darwin.os.sw_vers.load_os_release()

    assert mock_hub.grains.GRAINS.osrelease == "10.15.4"
    assert mock_hub.grains.GRAINS.osfullname == "Mac OS X 10.15.4"
    assert mock_hub.grains.GRAINS.osrelease_info == (10, 15, 4)
    assert mock_hub.grains.GRAINS.osmajorrelease == 10
    assert mock_hub.grains.GRAINS.os == "macOS"
    assert mock_hub.grains.GRAINS.oscodename == "Catalina"
    assert mock_hub.grains.GRAINS.osfinger == "Mac OS X 10.15.4-10"
