from dict_tools import data
import pytest

SCUTIL_DATA = """
DNS configuration

resolver #1
  nameserver[0] : 192.168.1.1
  if_index : 4 (en0)
  flags    : Request A records
  reach    : 0x00020002 (Reachable,Directly Reachable Address)

resolver #2
  domain   : local
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300000

resolver #3
  domain   : 123.123.in-addr.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300200

resolver #4
  domain   : 8.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300400

resolver #5
  domain   : 9.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300600

resolver #6
  domain   : a.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 300800

resolver #7
  domain   : b.e.f.ip6.arpa
  options  : mdns
  timeout  : 5
  flags    : Request A records
  reach    : 0x00000000 (Not Reachable)
  order    : 301000

DNS configuration (for scoped queries)

resolver #1
  nameserver[0] : 192.168.1.1
  if_index : 4 (en0)
  flags    : Scoped, Request A records
  reach    : 0x00020002 (Reachable,Directly Reachable Address)"""


@pytest.mark.asyncio
async def test_load_scutil_dns(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": SCUTIL_DATA})
    mock_hub.grains.darwin.net.dns.load_scutil_dns = (
        hub.grains.darwin.net.dns.load_scutil_dns
    )
    await mock_hub.grains.darwin.net.dns.load_scutil_dns()

    assert mock_hub.grains.GRAINS.dns.flags == ("Request A records", "Scoped")
    assert mock_hub.grains.GRAINS.dns.nameservers == ("192.168.1.1",)
    assert mock_hub.grains.GRAINS.dns.options == tuple()
    assert mock_hub.grains.GRAINS.dns.domain == tuple()
    assert mock_hub.grains.GRAINS.dns.search == tuple()
    assert mock_hub.grains.GRAINS.dns.sortlist == ("192.168.1.0/24",)
    assert mock_hub.grains.GRAINS.dns.ip4_nameservers == ("192.168.1.1",)
    assert mock_hub.grains.GRAINS.dns.ip6_nameservers == tuple()
