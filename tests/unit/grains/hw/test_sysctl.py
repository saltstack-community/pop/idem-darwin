from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_hwdata(mock_hub, hub):

    ret = "MacBookPro14,1"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": ret})
    mock_hub.grains.darwin.hw.sysctl.load_hwdata = (
        hub.grains.darwin.hw.sysctl.load_hwdata
    )
    await mock_hub.grains.darwin.hw.sysctl.load_hwdata()

    assert mock_hub.grains.GRAINS.productname == "MacBookPro14,1"
