import pytest
import mock


@pytest.mark.asyncio
async def test_load_pid(mock_hub, hub):

    ret = 8972358735
    with mock.patch("os.getpid", return_value=ret):
        mock_hub.grains.darwin.proc.pid.load_pid = hub.grains.darwin.proc.pid.load_pid
        await mock_hub.grains.darwin.proc.pid.load_pid()

    assert mock_hub.grains.GRAINS.pid == ret
