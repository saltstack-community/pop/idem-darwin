import pytest
import mock


@pytest.mark.asyncio
async def test_load_user(mock_hub, hub):

    with mock.patch("os.geteuid", return_value=99):
        ret = lambda: 0
        ret.pw_name = "test"
        with mock.patch("pwd.getpwuid", return_value=ret):
            mock_hub.grains.darwin.proc.user.load_user = (
                hub.grains.darwin.proc.user.load_user
            )
            await mock_hub.grains.darwin.proc.user.load_user()

    assert mock_hub.grains.GRAINS.uid == 99
    assert mock_hub.grains.GRAINS.username == "test"
