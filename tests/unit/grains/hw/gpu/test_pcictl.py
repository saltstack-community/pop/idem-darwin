from dict_tools import data
import pytest

PCIUTIL_DATA = """
Graphics/Displays:

    Intel Iris Plus Graphics 640:

      Chipset Model: Intel Iris Plus Graphics 640
      Type: GPU
      Bus: Built-In
      VRAM (Dynamic, Max): 1536 MB
      Vendor: Intel
      Device ID: 0x5926
      Revision ID: 0x0006
      Metal: Supported, feature set macOS GPUFamily2 v1
      Displays:
        Color LCD:
          Display Type: Built-In Retina LCD
          Resolution: 2560 x 1600 Retina
          Framebuffer Depth: 24-Bit Color (ARGB8888)
          Main Display: Yes
          Mirror: Off
          Online: Yes
          Automatically Adjust Brightness: No
          Connection Type: Internal
"""


@pytest.mark.asyncio
async def test_load_data(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PCIUTIL_DATA})
    mock_hub.grains.darwin.hw.gpu.pcictl.load_data = (
        hub.grains.darwin.hw.gpu.pcictl.load_data
    )
    await mock_hub.grains.darwin.hw.gpu.pcictl.load_data()
    assert mock_hub.grains.GRAINS.gpus == (
        {"model": "Iris Plus Graphics 640", "vendor": "intel"},
    )
    assert mock_hub.grains.GRAINS.num_gpus == 1
