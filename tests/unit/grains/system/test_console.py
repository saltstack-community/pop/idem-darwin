import pytest
import mock


@pytest.mark.asyncio
async def test_load_console_user(mock_hub, hub):

    ret = lambda: 0
    ret.pw_name = "test"
    with mock.patch("os.path.exists", return_value=True):
        with mock.patch("os.stat", return_value=(0, 1, 2, 3, 99)):
            with mock.patch("pwd.getpwuid", return_value=ret):
                mock_hub.grains.darwin.system.console.load_console_user = (
                    hub.grains.darwin.system.console.load_console_user
                )
                await mock_hub.grains.darwin.system.console.load_console_user()

    assert mock_hub.grains.GRAINS.console_user == 99
    assert mock_hub.grains.GRAINS.console_username == "test"


@pytest.mark.asyncio
async def test_load_console_user_root(mock_hub, hub):

    with mock.patch("os.path.exists", return_value=False):
        mock_hub.grains.darwin.system.console.load_console_user = (
            hub.grains.darwin.system.console.load_console_user
        )
        await mock_hub.grains.darwin.system.console.load_console_user()

    assert mock_hub.grains.GRAINS.console_user == 0
    assert mock_hub.grains.GRAINS.console_username == "root"
