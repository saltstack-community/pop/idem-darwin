import pytest


@pytest.mark.asyncio
async def test_load_defaults(mock_hub):
    # Hard coded grains, these should never change
    assert mock_hub.grains.GRAINS.init == "launchd"
    assert mock_hub.grains.GRAINS.osmanufacturer == "Apple Inc."
    assert mock_hub.grains.GRAINS.manufacturer == "Apple Inc."
    assert mock_hub.grains.GRAINS.os_family == "MacOS"
    assert mock_hub.grains.GRAINS.ps == "ps auxwww"
