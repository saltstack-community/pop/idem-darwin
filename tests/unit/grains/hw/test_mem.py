from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_mem(mock_hub, hub):

    ret = "8589934592"
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": ret})
    mock_hub.grains.darwin.hw.mem.load_mem = hub.grains.darwin.hw.mem.load_mem
    await mock_hub.grains.darwin.hw.mem.load_mem()
    assert mock_hub.grains.GRAINS.mem_total == int(ret) // 1024 // 1024


@pytest.mark.asyncio
async def test_load_swap(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "total = 1024.00M  used = 541.75M  free = 482.25M  (encrypted)"}
    )
    mock_hub.grains.darwin.hw.mem.load_swap = hub.grains.darwin.hw.mem.load_swap
    await mock_hub.grains.darwin.hw.mem.load_swap()
    assert mock_hub.grains.GRAINS.swap_total == 1024
