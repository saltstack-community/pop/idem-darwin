import pytest
import time
import mock


@pytest.mark.asyncio
async def test_load_info(mock_hub, hub):

    with mock.patch("locale.getdefaultlocale", return_value=("test_lang", "test_enc")):
        mock_hub.grains.darwin.system.locale.load_info = (
            hub.grains.darwin.system.locale.load_info
        )
        await mock_hub.grains.darwin.system.locale.load_info()

    assert mock_hub.grains.GRAINS.locale_info.defaultencoding == "test_enc"
    assert mock_hub.grains.GRAINS.locale_info.defaultlanguage == "test_lang"


@pytest.mark.asyncio
async def test_load_info(mock_hub, hub):

    with mock.patch("sys.getdefaultencoding", return_value="test"):
        mock_hub.grains.darwin.system.locale.load_default_encoding = (
            hub.grains.darwin.system.locale.load_default_encoding
        )
        await mock_hub.grains.darwin.system.locale.load_default_encoding()

    assert mock_hub.grains.GRAINS.locale_info.detectedencoding == "test"


@pytest.mark.asyncio
async def test_load_timezone(mock_hub, hub):

    val = time.tzname
    time.tzname = ("ZZZ", "ZZZ")
    mock_hub.grains.darwin.system.locale.load_timezone = (
        hub.grains.darwin.system.locale.load_timezone
    )
    await mock_hub.grains.darwin.system.locale.load_timezone()
    time.tzname = val

    assert mock_hub.grains.GRAINS.locale_info.timezone == "ZZZ"
