import pytest
import mock


@pytest.mark.asyncio
async def test_load_shell(mock_hub, hub):

    ret = "/bin/test_shell"
    with mock.patch("os.environ.get", return_value=ret):
        mock_hub.grains.darwin.system.shell.load_shell = (
            hub.grains.darwin.system.shell.load_shell
        )
        await mock_hub.grains.darwin.system.shell.load_shell()

    assert mock_hub.grains.GRAINS.shell == ret
