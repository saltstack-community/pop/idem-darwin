from dict_tools import data
import pytest


@pytest.mark.asyncio
async def test_load_computer_name(mock_hub, hub):

    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test"})

    mock_hub.grains.darwin.os.computer.load_computer_name = (
        hub.grains.darwin.os.computer.load_computer_name
    )

    await mock_hub.grains.darwin.os.computer.load_computer_name()

    assert mock_hub.grains.GRAINS.computer_name == "test"
