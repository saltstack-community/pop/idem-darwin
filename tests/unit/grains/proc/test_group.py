import pytest
import mock


@pytest.mark.asyncio
async def test_load_group(mock_hub, hub):

    with mock.patch("os.getegid", return_value=99):
        ret = lambda: 0
        ret.gr_name = "test"
        with mock.patch("grp.getgrgid", return_value=ret):
            mock_hub.grains.darwin.proc.group.load_group = (
                hub.grains.darwin.proc.group.load_group
            )
            await mock_hub.grains.darwin.proc.group.load_group()

    assert mock_hub.grains.GRAINS.gid == 99
    assert mock_hub.grains.GRAINS.groupname == "test"
